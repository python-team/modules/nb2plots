nb2plots (0.7.2-3) unstable; urgency=medium

  * adopt package inside DPT (Closes: #1064947)
  * deduplicate BSD-2-clause license, add some missing tags

 -- Alexandre Detiste <tchet@debian.org>  Sat, 16 Mar 2024 15:12:36 +0100

nb2plots (0.7.2-2) unstable; urgency=medium

  * orphan while waiting for RM

 -- Sandro Tosi <morph@debian.org>  Sat, 02 Mar 2024 16:54:35 -0500

nb2plots (0.7.2-1) unstable; urgency=medium

  [ Alexandre Detiste ]
  * New upstream release

  [ Sandro Tosi ]
  * debian/control
    - drop six from b-d, no longer needed; Closes: #1053999
    - run wrap-and-sort
    - add pandoc, ipykernel, nbconvert, nbformat, pytest to b-d, needed by
      tests
    - bump Standards-Version to 4.6.2 (no changes needed)
  * debian/rules
    - run tests at build time
  * debian/patches/PR36.patch
    - use absolute imports, allowing to run from_notebook via adt
  * run autopkgtests via autopkgtest-pkg-pybuild; Closes: #1035181, #1036519

 -- Sandro Tosi <morph@debian.org>  Wed, 24 Jan 2024 02:51:08 -0500

nb2plots (0.6.1-1) unstable; urgency=medium

  * New upstream release
  * debian/watch
    - track github releases
  * Drop patches, merged upstream

 -- Sandro Tosi <morph@debian.org>  Wed, 22 Jun 2022 23:53:38 -0400

nb2plots (0.6-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address
  * debian/copyright
    - extend packaging copyright years
  * debian/control
    - bump Standards-Version to 4.6.0.1 (no changes needed)
  * debian/patches/PR14.patch
    - import upstream PR to import Sequence properly; Closes: #1008261

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 13.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.1, no changes needed.
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on python3-six.

 -- Sandro Tosi <morph@debian.org>  Wed, 04 May 2022 23:00:14 -0400

nb2plots (0.6-2) unstable; urgency=medium

  [ Sandro Tosi ]
  * debian/control
    - add nbformat to depends, needed by the ipython shim module
    - bump Standards-Version to 4.4.1 (no changes needed)
  * Drop python2 support; Closes: #937116
  * debian/copyright
    - extrend packaging copyright years

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

 -- Sandro Tosi <morph@debian.org>  Tue, 12 Nov 2019 13:04:41 -0500

nb2plots (0.6-1) unstable; urgency=low

  * Initial release; Closes: #895717

 -- Sandro Tosi <morph@debian.org>  Sun, 15 Apr 2018 04:27:23 +0000
